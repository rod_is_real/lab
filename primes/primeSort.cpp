#include <iostream>

int main()
{
  int x;
  bool isPrime;
  std::cout << "Enter an integer: ";
  std::cin >> x;
  std::cout << "Prime numbers to " << x << ": ";
  for (int i=2;i<=x;i++){
    isPrime=true;
    for (int j=2;j<i && isPrime ;j++){
      if (!(i%j))
	isPrime=false;
    }
    if (isPrime)
      std::cout << i << " ";
  }
  std::cout << "\n";
  return 0;
}
