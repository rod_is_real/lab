#ifndef _CUSTOMERCOUNTER_H_
#define _CUSTOMERCOUNTER_H_

#include <string>
#include <iostream>
class CustomerCounter {
	private:
		int maximum_customers;
		int customer_count;
	public:
  		CustomerCounter(int maximun_customers);
  		void add(int number_customers);
  		void subtract(int number_customers);
		void printCustomers();
		virtual ~CustomerCounter();
}; 

#endif
