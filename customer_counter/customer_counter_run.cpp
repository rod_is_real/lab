#include "customer_counter.h"
#include <iostream>


int main(){
	CustomerCounter test_shop_name = CustomerCounter(100);
	test_shop_name.printCustomers();
	test_shop_name.add(500);
	test_shop_name.printCustomers();
	test_shop_name.subtract(110);
	test_shop_name.printCustomers();
}
