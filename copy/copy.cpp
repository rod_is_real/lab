#include <iostream>
#include <fstream>
int main(int argc, char *argv[3]){
  if (argc < 3){
    std::cout << "Usage: " << argv[0]
	      << " input-filename" << " output-filename\n";
    exit(1);
  }

  std::ifstream in_file;
  std::ofstream out_file;
  std::string line;
  
  in_file.open(argv[1]);
  out_file.open(argv[2]);
  
  while(getline(in_file, line))
    {
      out_file << line;
      out_file << "\n";
    }
  
  in_file.close();
  out_file.close();
  
  return 0;
}
