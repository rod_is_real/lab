#include "person.h"

Person::Person(std::string name)
{
  this->name = name;
}

void Person::befriend(Person * person)
{
	this->friends.push_back(person);
}

std::string Person::getName()
{
	return this->name;
}
void Person::unfriend(Person * person)
{
	for (int i=0; i < this->friends.size(); i++)
	{
		if (this->friends[i]->getName() == person->getName())
		{
			this->friends.erase(this->friends.begin() + i);
			return;
		}
	}
}

void Person::printDetails()
{
	std::cout << "This person's name is: " << this->getName() << std::endl;
	std::cout << "This person's friends are: \n";
	for (Person* p : this->friends)
	{
		std::cout << p->getName() << std::endl;
	}
}
