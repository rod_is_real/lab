#include <iostream>
#include "person.h"

int main()
{
	Person person1 = Person("Rod The God");
	Person person2 = Person("Jeff Bezos");
	Person person3 = Person("Elon Musk");
	Person person4 = Person("Bill Gates");

	person1.befriend(&person2);
	person1.befriend(&person3);
	person1.befriend(&person4);
	person1.printDetails();

	person1.unfriend(&person4);
	person1.unfriend(&person2);
	person1.printDetails();

	person3.befriend(&person1);
	person3.printDetails();


}
