#include <iostream>

int cube(int n){
  return n*n*n;
}

int main(){
  int a;
  std::cout << "Enter an integer: ";
  std::cin >> a;
  std::cout << "The cube of the number is "<< cube(a) << "\n";
}
