#include "numberconversion.h"

int roman_to_int(std::string roman){
	map<char,int> rmap;
	rmap['I'] = 1;
	rmap['V'] = 5;
	rmap['X'] = 10;
	rmap['L'] = 50;
	rmap['C'] = 100;
	rmap['D'] = 500;
	rmap['M'] = 1000;
	int decimal = 0;
	int i = 0;
	if (roman.length() <= 1){
		return rmap[roman.at(0)];
	}
	else {
		while (i < roman.size()){
			if (rmap[roman[i]] < rmap[roman[i + 1]]){
				decimal += rmap[roman[i + 1]] - rmap[roman[i]];
				i += 2;
			}
			else {
				decimal += rmap[roman[i]];
				i ++;
			}
		}
		return decimal;
	}
}

std::string int_to_roman(int decimal)
{
	int values[] = {1000, 900, 500, 400, 100, 90, 50, 40, 10,9 ,5 ,4 , 1};
	std::string roman_literals[] = {"M","CM","D","CD","C","XC","L","XL","X","IX","V","IV","I"};
	std::string roman = "";
	int len = *(&values + 1) - values;
	for(int i = 0; i < len; i ++){
		while (decimal >= values[i]) {
			decimal -= values[i];
			roman.append(roman_literals[i]);
		}
	}

	return roman;
}
