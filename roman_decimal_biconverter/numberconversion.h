#ifndef _NUMBERCONVERSION_H_
#define _NUMBERCONVERSION_H_

#include <string>
#include <map>
using namespace std;

/*
  Convert a number from the Roman numeral representation to an
  integer.
  @param roman_numeral the Roman numeral representation of the number
  to be converted.
  @return integer representing the Roman numerals.
*/
int roman_to_int(std::string roman);

/*
  Convert a number from an integer to the Roman numeral
  representation.
  @param number the integer to be converted to Roman numerals.
  @return Roman numeral representation of the number.
*/
std::string int_to_roman(int decimal);

#endif
