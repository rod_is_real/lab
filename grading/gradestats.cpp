#include <iostream>

int main()
{
  const int MAX_SIZE = 100;
  int grades[MAX_SIZE];
  int size=0;
  int total=0;
  int lastGrade=0;
  
  while ((size<MAX_SIZE) && (lastGrade!=-1))
  {
    std::cout << "Enter a grade (or -1 to exit): ";
    std::cin >> lastGrade;
    if (lastGrade!=-1)
    {
      grades[size]=lastGrade;
      total += lastGrade;
      size++;
    }      
  }
  std::cout << "Average: " << (total/size)  << "\n";
}
