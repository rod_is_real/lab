#include <iostream>

int main()
{
  int x,y,total=0;
  std::cout << "Enter two numbers: ";
  std::cin >> x >> y;
  if (x == y) {total=0;}
  else{
    for (int i=x;i<=y;i++)
      total= total + i;
  }
  std::cout << "Sum from " << x << " to " << y << " is " << total << " ." << std::endl;
  return 0;
}
