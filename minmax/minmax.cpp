#include <iostream>

int main()
{
  const int MAX_SIZE=10;
  int arr[MAX_SIZE];
  int maximum=0;
  int minimum=100;
  srand(time(NULL));
  std::cout << "Random numbers: ";
  for (int i=0;i<MAX_SIZE;i++) {
    arr[i]=rand()%100;
    if (arr[i]>maximum){maximum = arr[i];}
    if (arr[i]<minimum){minimum = arr[i];}
    std::cout << arr[i] << " ";
  }
  std::cout << "\n";
  std::cout << "Maximum is " << maximum << " and minimum is " << minimum << "\n";
  return 0;
}
