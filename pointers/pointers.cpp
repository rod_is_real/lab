#include <iostream>

int main()
{
  // variable
  double num = 2.4;
  std::cout << "Variable value: " << num << "; address: " << &num << ". \n";
  //pointer
  double* num_ptr =  &num;
  std::cout << "Pointer value: " << num_ptr << "; address: " << &num_ptr << "; deference: " << *num_ptr << ". \n";
  
  num_ptr = nullptr;
  
  return 0;
}
