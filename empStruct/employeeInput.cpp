#include <iostream>

struct Employee {
  int id;
  std::string firstName;
  std::string lastName;
  std::string email;
  std::string phoneNumber;
  double salary;
  struct Employee * manager;
};


void setEmployee(Employee* employee, int id, std::string firstName, std::string lastName, std::string email, std::string phoneNumber, double salary)
{
  employee->id=id;
  employee->firstName=firstName;
  employee->lastName=lastName;
  employee->email=email;
  employee->phoneNumber=phoneNumber;
  employee->salary=salary;
  employee->manager=NULL;
}

void fillStaffFromUser(Employee* employee)
{
  int id;
  std::string firstName;
  std::string lastName;
  std::string email;
  std::string phoneNumber;
  double salary;

  std::cout << "\nID: ";
  std::cin >> id;
  std::cout << "\nFirst Name: ";
  std::cin >> firstName;
  std::cout << "\nLast Name: ";
  std::cin >> lastName;
  std::cout << "\nE-mail: ";
  std::cin >> email;
  std::cout << "\nPhone Number: ";
  std::cin >> phoneNumber;
  std::cout << "\nSalary: ";
  std::cin >> salary;
  std::cout << "\n";
  setEmployee(employee, id, firstName, lastName, email, phoneNumber, salary);
}

void printStaff(Employee* employees, int numberStaff)
{
  int i=0;
  std::cout << "ID \t Name\n";
  for(i=0;i<numberStaff;i++)
    {
      std::cout << employees[i].id << "\t" << employees[i].firstName << std::endl;
    }
}

int main()
{
  int numberStaff=0;
  int i =0;
  
  std::cout << "Please enter the number of staff: ";
  std::cin >> numberStaff;
  
  Employee *employees = new Employee[numberStaff];

  for (i=0; i < numberStaff;i++)
    {
      fillStaffFromUser(&employees[i]);
    }
  printStaff(employees,numberStaff);
  delete [] employees;
}
