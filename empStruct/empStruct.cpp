#include <iostream>
#include <string>

struct Employee {
  int id;
  std::string firstName;
  std::string lastName;
  std::string email;
  std::string phoneNumber;
  Employee * manager;
  double salary;
};

int main()
{
  Employee employee1;
  Employee employee2;
  Employee manager;

  employee1.id=1;
  employee1.firstName="Joe";
  employee1.lastName="Zeff";
  employee1.email="jeff@company.com";
  employee1.phoneNumber="123123123123";
  employee1.salary=100000.01;
  employee1.manager=&manager;

  employee2.id=2;
  employee2.firstName="An";
  employee2.lastName="Frank";
  employee2.email="annie@company.com";
  employee2.phoneNumber="321321321321";
  employee2.salary=99000.85;
  employee2.manager=&manager;

  manager.id=3;
  manager.firstName="Carl";
  manager.lastName="Boss";
  manager.email="dboss@company.com";
  manager.phoneNumber="666666666666";
  manager.salary=1000000;
  manager.manager=&manager;
  
}

