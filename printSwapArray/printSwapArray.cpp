#include <iostream>

void printArray(int arr[], int size){
  std::cout << "[";
  for (int i=0;i<size-1;i++){
    std::cout << arr[i] << ", ";
  }
  std::cout << arr[size-1] << "]\n";
}

void swapArray(int arr[], int x, int y){
  int tmp;
  tmp = arr[x];
  arr[x] = arr[y];
  arr[y] = tmp;
}

int main() {
  int dataSize = 5;
  int data[] = {1, 2, 3, 4, 5, 6, 7, 8};
  printArray(data, dataSize);
  swapArray(data, 0, 1);
  printArray(data, dataSize);
  return 0;
}
